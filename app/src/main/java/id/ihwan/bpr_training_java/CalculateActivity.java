package id.ihwan.bpr_training_java;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculateActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText angka1, angka2;
    private Button tambah, kurang;
    private TextView total;

    private static final String STATE_RESULT = "state_result";

    //menyimpan state
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_RESULT, total.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);

        angka1 = findViewById(R.id.angka_pertama);
        angka2 = findViewById(R.id.angka_kedua);

        total = findViewById(R.id.hasil_angka);

        tambah = findViewById(R.id.tombol_tambah);
        kurang = findViewById(R.id.tombol_kurang);

        tambah.setOnClickListener(this);
        kurang.setOnClickListener(this);

        if (savedInstanceState != null){
            String hasil = savedInstanceState.getString(STATE_RESULT);
            total.setText(hasil);
        }



    }


    @Override
    public void onClick(View v) {




        boolean isEmptyFields = false;

        switch (v.getId()){
            case R.id.tombol_tambah:
                String bilangan1 = angka1.getText().toString().trim();
                String bilangan2 = angka2.getText().toString().trim();

                if (TextUtils.isEmpty(bilangan1)){
                    isEmptyFields = true;
                    angka1.setError("isi dulu!");
                }
                if (TextUtils.isEmpty(bilangan2)){
                    isEmptyFields = true;
                    angka2.setError("isi dulu!");
                }
                if (!isEmptyFields){
                    double bil1 = Double.parseDouble(bilangan1);
                    double bil2 = Double.parseDouble(bilangan2);
                    double hasil = bil1 + bil2;

                    total.setText("Hasil = "+String.valueOf(hasil));
                }

                break;
            case R.id.tombol_kurang:

//                double hasil2 = bil1 - bil2;

//                total.setText("Hasil = " + String.valueOf(hasil2));

                break;
        }
    }
}
